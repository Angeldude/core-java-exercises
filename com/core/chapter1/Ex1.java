package com.core.chapter1;

import java.util.Scanner;
import java.lang.Integer;

public class Ex1 {

  public static void main(String[] args) {

    Scanner in =  new Scanner(System.in);
    System.out.println("Enter a number:" );

    int number = in.nextInt();

    System.out.println("The number, " + number + ", converted to:");
    System.out.printf("Octal: %o\n", number);
    System.out.printf("Hexadecimal: %x\n", number);
    System.out.println("Binary: " + Integer.toBinaryString(number));
    System.out.printf("Reciprocal Hexadecimal: %a\n", (1.0/number));
   }
}
