package com.core.chapter1;

import java.util.Scanner;


class Ex7
{
  public static void main(String[] args)
  {
    /* “Write a program that reads in two numbers between 0 and 65535,
    stores them in short variables, and computes their unsigned sum,
    difference, product, quotient, and remainder, without converting them to
    int.”*/
      
      short num1, num2;
      
      Scanner input;
      input = new Scanner(System.in);
      
      System.out.println("Enter a number between 0 and 65535");
      num1 = (short)input.nextInt();
      
      System.out.println("Enter a second number");
      num2 = (short)input.nextInt();

      System.out.println("Here's the sum");
      System.out.println((num1 + num2) < 0 ? ~(num1+num2): num1 + num2);
      
      System.out.println("Here's the difference");
      System.out.println(num1 - num2);
      
      System.out.println("Here's the product");
      System.out.println(num1 * num2);
      
      System.out.println("Here's the quotient");
      System.out.println(num1 / num2);
      
      System.out.println("Here's the remainder");
      System.out.println(num1 % num2);
  }
}
