package com.core.chapter1;

public class Ex5
{
  public static void main(String[] args)
  {
    // cast a double from largest possible int
    int max = (int)Math.pow(2,35);
    System.out.println(max);

    /* it turns out when casting a double to an int that exceeds the
    int's max size, it gets set to the max int number*/
  }
}
