package com.core.chapter1;

import java.util.Scanner;

public class Ex3
{
  public static void main(String[] args)
  {
    Scanner in = new Scanner(System.in);
    int one,two,three;

    System.out.println("Enter a number:");
    one = in.nextInt();
    System.out.println("Enter a second number:");
    two = in.nextInt();
    System.out.println("Enter a final number:");
    three = in.nextInt();

    int almost = Math.max(Math.max(one, two), three);
    System.out.println("Largest number is " + almost);
  }

}
