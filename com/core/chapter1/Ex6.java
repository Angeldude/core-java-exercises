package com.core.chapter1;

import java.math.BigInteger;

class Ex6
{
  public static void main(String[] args)
  {

    BigInteger result = new BigInteger("1");

    for(int n = 1; n <= 1000; n++)
    {
      result =  result.multiply(BigInteger.valueOf(n));
    }

    System.out.println("The factorial of 1000 is");
    System.out.println(result);

  }
}
