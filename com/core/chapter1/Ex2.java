package com.core.chapter1;

import java.util.Scanner;

public class Ex2 {

  public static void main(String[] args) {

    Scanner in  = new Scanner(System.in);

    System.out.println("Enter a degree angle:");
    int num = in.nextInt();

    System.out.println("This is " + (Math.floorMod(num, 360)) + " degrees.");
  }
}
